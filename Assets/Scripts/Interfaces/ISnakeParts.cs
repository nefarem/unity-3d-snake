// Interfejsy 

public interface IMoving
{
    void Move();
}

public interface IMovingBody
{
    void Move(int snakeBodyIndex);
}

public interface ISetSprite
{
    void SetSprite();
}