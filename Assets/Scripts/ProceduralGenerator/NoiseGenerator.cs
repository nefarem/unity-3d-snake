using UnityEngine;

public class NoiseGenerator : MonoBehaviour
{
    // Rozmiar planszy X i Y
    public int sizeX;
    public int sizeY;
    public GameObject groundPrefab; // Prefab powierzchni
    public GameObject wallPrefab; // Prefab ściany

    [Header("Noise settings")]
    public int Octaves; 
    [Range(0, 1)]
    public float Persistance;
    public float Lacunarity;
    public float NoiseScale;
    public Vector2 Offset;
    
    public void GenerateMap(int mapSeed)
    {
       PlayerPrefs.SetInt("MapSeed", mapSeed);

       GameObject mapObjects = new GameObject("Map");
         
        var noiseMap = Noise.GenerateNoiseMap(sizeX, sizeY, mapSeed, NoiseScale, Octaves, Persistance, Lacunarity, Offset);

        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                var height = noiseMap[y * sizeY + x];

                if (height <= 0.2f && (x != 2 && y != 2) && (x != 2 && y != 3) && (x != 2 && y != 4))
                {
                    GameObject wall = Instantiate(wallPrefab, new Vector2(0.16f * x, 0.17f * y), Quaternion.identity);
                    wall.transform.parent = mapObjects.transform;
                    wall.layer = 6;
                }
                else if (x == 0) 
                {
                    GameObject wall = Instantiate(wallPrefab, new Vector2(0.16f * x, 0.17f * y), Quaternion.identity);
                    wall.transform.parent = mapObjects.transform;
                    wall.layer = 6;
                }

                else if (y == 0)
                {
                    GameObject wall = Instantiate(wallPrefab, new Vector2(0.16f * x, 0.17f * y), Quaternion.identity);
                    wall.transform.rotation = Quaternion.Euler(0,0,90);
                    wall.transform.parent = mapObjects.transform;
                    wall.layer = 6;
                }
                else if (x == sizeY - 1)
                {
                    GameObject wall = Instantiate(wallPrefab, new Vector2(0.16f * x, 0.17f * y), Quaternion.identity);
                    wall.transform.parent = mapObjects.transform;
                    wall.layer = 6;
                }
                else if (y == sizeY - 1)
                {
                    GameObject wall = Instantiate(wallPrefab, new Vector2(0.16f * x, 0.17f * y), Quaternion.identity);
                    wall.transform.rotation = Quaternion.Euler(0,0,90);
                    wall.transform.parent = mapObjects.transform;
                    wall.layer = 6;
                }
                else
                {
                    GameObject ground = Instantiate(groundPrefab, new Vector2(0.16f * x, 0.16f * y), Quaternion.identity);
                    ground.name = "Ground" + x + y;

                    ground.GetComponent<Node>().isAvailable = true;

                    ground.transform.parent = mapObjects.transform;                   
                }
            }
        }
    }
}
