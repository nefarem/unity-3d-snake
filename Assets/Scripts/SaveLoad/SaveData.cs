using System.Collections.Generic;

// Dane z mapy która będą następnie zapisane do pliku .save
[System.Serializable]
public class SaveData
{
    // Game version
    public string gameVersion;
    
    // Pozycja główy węża
    public SerializableVec2 snakeHeadPosition;

    // Pozycje ciała wężą
    public List<SerializableVec2> playerBodyPosition = new List<SerializableVec2>();

    // Pozycja ogona wężą
    public SerializableVec2 tailPosition;

    // Pozycja jedzenia
    public SerializableVec2 foodPosition;

    // Pozycja przeciwnika
    public SerializableVec2 enemyPosition;

    // Kierunek ruchu wężą
    public Snake.MoveDirection MoveDirection;

    // Liczba punktów
    public int score;
    
    // Highscore
    public int highscore;
    
    // Jezyk gry
    public int gameLanguage;
    
    // Glosnosc gry
    public float audioVolume;
    
    // Sterowanie
    public int[] snakeControl = new int [4];
    
    // Kontynuacja gry dostepna
    public bool canContinueGame;

    public SaveData()
    {
    }
    
    public SaveData(SerializableVec2 snakeHeadPosition, List<SerializableVec2> playerBodyPosition, SerializableVec2 tailPosition, SerializableVec2 foodPosition,
        SerializableVec2 enemyPosition, Snake.MoveDirection MoveDirection, int score, int[] snakeControl)
    {
        this.snakeHeadPosition = snakeHeadPosition;
        this.playerBodyPosition = playerBodyPosition;
        this.tailPosition = tailPosition;
        this.foodPosition = foodPosition;
        this.enemyPosition = enemyPosition;
        this.MoveDirection = MoveDirection;
        this.score = score;
        this.snakeControl = snakeControl;
    }
}
