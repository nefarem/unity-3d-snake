using UnityEngine;

// Pomocnicza klasa do serializacji Vector2
[System.Serializable]
public class SerializableVec2
{
    public float x;
    public float y;
    
    public SerializableVec2(float _x, float _y)
    {
        x = _x;
        y = _y;
    }

    public Vector2 GetVector2()
    {
        return new Vector2(x, y);
    }
}
