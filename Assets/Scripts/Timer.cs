﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float timerStartValue = 0.8f;
    public float timerValue;
    public float timeSpeed;

    // Licznik co ile wąż wykonuje ruch, wartość przyspieszana przy pomocy zmiennej timeSpeed która rośnie po każdym zjedzeniu
    void Update() 
    {
        timerValue = timerValue - Time.deltaTime - timeSpeed;

        if (timerValue <= 0f) timerValue = timerStartValue;
    }
}
