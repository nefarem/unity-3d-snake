using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip[] audioClips;
    
    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Change volume level in audio source component.
    /// </summary>
    /// <param name="volumeLevel"></param>
    public void SetVolumeLevel(float volumeLevel)
    {
        if (audioSource is null)
        {
            Managers.DebugManager.DebugLogError("Can't find AudioSource component!");
            return;
        }
        
        audioSource.volume = volumeLevel;
    }

    public float GetVolumeLevel()
    {
        return audioSource.volume;
    }

    /// <summary>
    /// Set mute on/off in audio source component.
    /// </summary>
    /// <param name="value"></param>
    public void ChangeMuteAudioValue(bool value)
    {
        if (audioSource is null)
        {
            Managers.DebugManager.DebugLogError("Can't find AudioSource component!");
            return;
        }
        
        audioSource.mute = value;
    }

    /// <summary>
    /// Change audio clip or change music in audio source component.
    /// </summary>
    /// <param name="music">"0" for menu, "1" for gameplay</param>
    public void ChangeClip(int musicIndex)
    {
        if (audioSource is null)
        {
            Managers.DebugManager.DebugLogError("Can't find AudioSource component!");
            return;
        }
        
        audioSource.clip = audioClips[musicIndex];       
        audioSource.Play();
    }
}
