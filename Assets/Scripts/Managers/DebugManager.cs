using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugManager : MonoBehaviour
{
    public void DebugLog(string log)
    {
        if (Managers.ConfigManager.debugMode)
            Debug.Log(log);
    }

    public void DebugLogError(string log)
    {
        if (Managers.ConfigManager.debugMode)
            Debug.LogError(log);  
    }
}
