using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    // Aktualizowanie informacji na interfejsie oraz po kliknięciu klawisza "P" wyświetlenie menu pauzy
    public Text scoreText; 
    public GameObject pauseMenu;

    int currentScore;
    int currentHighscore;
    
    void Start() 
    {
        currentScore = Managers.LevelManager.score;
        currentHighscore = Managers.LevelManager.highscore;
    }

    void Update() 
    {
        UpdateTextScore();

        if (Input.GetKeyDown(KeyCode.P) && !Managers.GameManager.isGamePause)
        {
            ActivePauseMenu();
        }
    }

    void UpdateTextScore()
    {
        if(currentScore != Managers.LevelManager.score || currentHighscore != Managers.LevelManager.highscore)
        {
            currentScore = Managers.LevelManager.score;
            currentHighscore = Managers.LevelManager.highscore;
            
            scoreText.text = "Score: " + Managers.LevelManager.score + " " + "Highscore: " + Managers.LevelManager.highscore;
        } 
    }

    void ActivePauseMenu()
    {
        pauseMenu.SetActive(true);
        Managers.GameManager.isGamePause = true;
    }
}
