using System;
using UnityEngine;

public enum LanguageTypes
{
    Polish = 0,
    English = 1,
    Czech = 2
}

public class MultiLanguageManager : MonoBehaviour
{
    public event Action<LanguageTypes, LanguageTypes> ChangeLanguage;
    public void OnChangeLanguage(LanguageTypes newLanguage, LanguageTypes oldLanguage) { ChangeLanguage?.Invoke(newLanguage, oldLanguage); }

    public string pathToLocalisationFile;
    
    private LanguageTypes currentLanguage;

    public LanguageTypes CurrentLanguage
    {
        get
        {
            return currentLanguage;
        }
    }
    
    /// <summary>
    /// Change language in game and invoke event.
    /// </summary>
    /// <param name="newLanguage"></param>
    public void SetLanguage(LanguageTypes newLanguage)
    {
        switch ((int)newLanguage)
        {
            case 0:
                Managers.MultiLanguageManager.pathToLocalisationFile = Application.dataPath + "/pl-PL.json";
                break;

            case 1:
                Managers.MultiLanguageManager.pathToLocalisationFile = Application.dataPath + "/en-UK.json";
                break;

            case 2:
                Managers.MultiLanguageManager.pathToLocalisationFile = Application.dataPath + "/cz-CZ.json";
                break;
        }
        
        LanguageTypes oldLanguage = currentLanguage;
        currentLanguage = newLanguage;
        
        Managers.ProfileManager.SaveProfile();
        
        OnChangeLanguage(oldLanguage, newLanguage);
    }

    /// <summary>
    /// Get string from JSON file using string key.
    /// </summary>
    /// <param name="languageKey"></param>
    /// <returns>string</returns>
    public string GetStringByKey(string languageStringKey)
    {
        string result = "";
        
        switch (languageStringKey)
        {
            case "continueGame":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).continueGame;
            break;

            case "newGame":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).newGame;
            break;

            case "changeLanguage":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).changeLanguage;
            break;

            case "exitGame":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).exitGame;
            break;

            case "snakeControl":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).snakeControl;
            break;   

            case "pauseMenu":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).pauseMenu;
            break;

            case "gameDesc1":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).gameDesc1;
            break;

            case "gameDesc2":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).gameDesc2;
            break;

            case "resetHighscoreAsk":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).resetHighscoreAsk;
            break;

            case "resetHighscoreYes":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).resetHighscoreYes;
            break;

            case "resetHighscoreNo":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).resetHighscoreNo;
            break;   

            case "gameVersion":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).gameVersion;
            break;   

            case "backToMenu":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).backToMenu;
            break;   

            case "youLose":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).youLose;
            break;   

            case "youWin":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).youWin;
            break;   

            case "resetGame":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).resetGame;
            break;       
           
            case "authorText":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).authorText + " Nefarem";
                break; 
            
            case "settingsButton":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).settingsButton;
                break;  
            
            case "changesButton":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).changesButton;
                break; 
  
            case "roadmapButton":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).roadmapButton;
                break;  
            
            case "multiplayerButton":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).multiplayerButton;
                break; 
            
            case "aboutButton":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).aboutButton;
                break;  
            
            case "settingsMoveUp":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).settingsMoveUp;
                break;  
            
            case "settingsMoveDown":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).settingsMoveDown;
                break;  
            
            case "settingsMoveLeft":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).settingsMoveLeft;
                break;  
            
            case "settingsMoveRight":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).settingsMoveRight;
                break;  
            
            case "settingsGameLanguage":
                result = LanguageStrings.CreateJSON(pathToLocalisationFile).settingsGameLanguage;
                break;  
            
            default:
                Managers.DebugManager.DebugLogError("This string language key don't exitst " + languageStringKey);
                break;
        }

        return result;
    }
}
