using UnityEngine;

public static class Managers
{
    private static GameManager gameManager;
    public static GameManager GameManager
    {
        get
        {
            if (gameManager is null)
                gameManager = GameObject.FindObjectOfType<GameManager>();

            return gameManager;        
        }
    }
    
    private static AudioManager audioManager;
    public static AudioManager AudioManager
    {
        get
        {
            if (audioManager is null)
                audioManager = GameObject.FindObjectOfType<AudioManager>();

            return audioManager;        
        }

    }
    
    private static DebugManager debugManager;
    public static DebugManager DebugManager
    {
        get
        {
            if (debugManager is null)
                debugManager = GameObject.FindObjectOfType<DebugManager>();

            return debugManager;          
        }
 
    }
    
    private static ConfigManager configManager;
    public static ConfigManager ConfigManager
    {
        get
        {
            if (configManager is null)
                configManager = GameObject.FindObjectOfType<ConfigManager>();

            return configManager;    
        }
    }
    
    private static MultiLanguageManager multiLanguageManager;
    public static MultiLanguageManager MultiLanguageManager
    {
        get
        {
            if (multiLanguageManager is null)
                multiLanguageManager = GameObject.FindObjectOfType<MultiLanguageManager>();

            return multiLanguageManager;    
        }
    }
    
    private static ProfileManager profileManager;
    public static ProfileManager ProfileManager
    {
        get
        {
            if (profileManager is null)
                profileManager = GameObject.FindObjectOfType<ProfileManager>();

            return profileManager;    
        }
    }
    
    private static LevelManager levelManager;
    public static LevelManager LevelManager
    {
        get
        {
            if (levelManager is null)
                levelManager = GameObject.FindObjectOfType<LevelManager>();

            return levelManager;    
        }
    }
}
