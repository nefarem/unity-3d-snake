using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject[] groundAvailableList; // Tablica z powierzchniami które są wolne i można na nich zrespić jedzenie

    public int score; // Liczba punktów
    public int highscore; // Najwyższy wynik
    
    private void Awake()
    {
        NoiseGenerator generator = GetComponent<NoiseGenerator>();
        
        // Jeżeli zmienna gameLoaded jest nierówna 1 to mapa będzie generowana, w przeciwnym razie zostanie ona wczytana 
        generator.GenerateMap(Random.seed);

        // Uzupełnienie tablicy obiektami z tagiem Ground
        groundAvailableList = GameObject.FindGameObjectsWithTag("Ground"); 
    }
    
    void Update() 
    {
        // Ustawienie nowej wartości higscore
        if (score > highscore)
            highscore = score;
    }
}
