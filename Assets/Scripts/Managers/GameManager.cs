﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject gameOverPanel; // Panel UI z wynikiem gry (wygrana albo nie)
    
    public bool isGamePause; // Czy gra jest zapauzowana
    public bool isGameOver; // Czy gra została zakończona z wynikiem (wygrana albo nie)
    public bool loadingFinish = false; // Czy gra ukończyla ładowania wszystkiego
    
    private void Awake()
    {
        StartCoroutine(InitGame());
    }



    void Update() 
    {
        if (!loadingFinish)
            return;
    }

    // Zakończenie gry z wynikiem wygrana albo nie
    public void GameOver(bool result, int score, int highscore)
    {
        isGameOver = true;
        
        gameOverPanel.SetActive(true); // Aktywowanie panelu z końcem gry

        // Zmiana tekstu wyniku gry
        //if (result) gameOverPanel.transform.GetChild(0).GetComponent<Text>().text = LanguageStrings.CreateJSON(GameLanguage.pathToLocalisationFile).youWin;
        //else gameOverPanel.transform.GetChild(0).GetComponent<Text>().text = LanguageStrings.CreateJSON(GameLanguage.pathToLocalisationFile).youLose;

        // Wyświetlenie punktow i najwyższego wyniku
        gameOverPanel.transform.GetChild(1).GetComponent<Text>().text = "Score: " + score + " " + "Highscore: " + highscore;
    }

    /*
     * Load GlobalManagers scene
     * Load game save data if exists
     */
    IEnumerator InitGame()
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("GlobalManagers", LoadSceneMode.Additive);

        // Wait until the asynchronous scene fully loads
        while (!asyncOperation.isDone)
            yield return null;
        
        loadingFinish = true;
        
        Managers.ProfileManager.LoadProfile();
        
        Managers.AudioManager.ChangeClip(0);
        
        Managers.DebugManager.DebugLog("Loading success!");
    }
}
