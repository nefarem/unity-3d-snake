using UnityEngine;

public class ConfigManager : MonoBehaviour
{
    public bool debugMode;
    public string gameVersion;

    public int upMoveKey = 0; // 0 - arrow, 1 - W
    public int downMoveKey = 0; // 0 - arrow, 1 - S
    public int leftMoveKey = 0; // 0 - arrow, 1 - A
    public int rightMoveKey = 0; // 0 - arrow, 1 - D
}
