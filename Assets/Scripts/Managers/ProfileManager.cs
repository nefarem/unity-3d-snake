using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class ProfileManager : MonoBehaviour
{
    public PlayerProfile playerProfile;

    public void CreateNewProfile()
    {
        PlayerPrefs.DeleteAll();
        
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
            File.Delete(Application.persistentDataPath + "/gamesave.save");
        
        playerProfile = null;
        playerProfile = new PlayerProfile(new SaveData());

        Debug.Log("CreateNewProfile");
    }

    public void LoadProfile()
    {
        // Wczytywanie pliku i tworzenie obiektu klasy SaveData
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {   
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            SaveData saveData = (SaveData)bf.Deserialize(file);
            file.Close();
            
            /*
              // Pozycja główy węża
    public SerializableVec2 snakeHeadPosition;

    // Pozycje ciała wężą
    public List<SerializableVec2> playerBodyPosition = new List<SerializableVec2>();

    // Pozycja ogona wężą
    public SerializableVec2 tailPosition;

    // Pozycja jedzenia
    public SerializableVec2 foodPosition;

    // Pozycja przeciwnika
    public SerializableVec2 enemyPosition;

    // Kierunek ruchu wężą
    public Snake.MoveDirection MoveDirection;

    // Liczba punktów
    public int score;
    
    // Sterowanie
    public int[] snakeControl = new int [4];
             
             */
            
         
            /*
            // Szukanie głowy wężą
            // Zmienianie pozycji węża oraz ustalanie kierunku ruchu
            foreach (GameObject item in GameObject.FindGameObjectsWithTag("Player"))
            {
                if (item.GetComponent<Snake>())
                {
                    snakeObject = item;
                    Snake snake = snakeObject.GetComponent<Snake>();

                    snakeObject.transform.position = saveData.snakeHeadPosition.GetVector2();

                    switch (saveData.MoveDirection)
                    {
                        case Snake.MoveDirection.up:
                            snake.MoveDirectionEnum = Snake.MoveDirection.up;
                        break;

                        case Snake.MoveDirection.down:
                            snake.MoveDirectionEnum = Snake.MoveDirection.down;
                        break;

                        case Snake.MoveDirection.left:
                            snake.MoveDirectionEnum = Snake.MoveDirection.left;
                        break;

                        case Snake.MoveDirection.right:
                            snake.MoveDirectionEnum = Snake.MoveDirection.right;
                        break;
                    }

                    snake.SetSprite();
                    break;
                } 

                if (item.GetComponent<Tail>()) item.transform.position = saveData.tailPosition.GetVector2();
            }
   
            int i = 0;

            // Tworzenie części ciała wężą
            foreach (var item in saveData.playerBodyPosition)
            {
                Vector2 bodyPosition = saveData.playerBodyPosition[i].GetVector2();

                GameObject body = Instantiate(snakeBodyPrefab, bodyPosition, Quaternion.identity);
                snakeObject.GetComponent<Snake>().snakeBody.Add(body);
                i++;
            }

            Vector2 foodPosition = saveData.foodPosition.GetVector2();
            Instantiate(foodPrefab, foodPosition, Quaternion.identity);     

            Vector2 enemyPosition = saveData.enemyPosition.GetVector2();
            Instantiate(enemyPrefab, enemyPosition, Quaternion.identity); 
            */
            
            
            // Sterowanie
            Managers.ConfigManager.upMoveKey = saveData.snakeControl[0];
            Managers.ConfigManager.downMoveKey = saveData.snakeControl[1];
            Managers.ConfigManager.leftMoveKey = saveData.snakeControl[2];
            Managers.ConfigManager.rightMoveKey = saveData.snakeControl[3];
            
            // Glosnosc gry
            Managers.AudioManager.SetVolumeLevel(saveData.audioVolume);
            
            // Jezyk gry
            Managers.MultiLanguageManager.SetLanguage((LanguageTypes)saveData.gameLanguage);
            
            playerProfile = new PlayerProfile(saveData);
        }
        else
            CreateNewProfile();
    }

    public void SaveProfile()
    {
        SaveData saveData = new SaveData();
        
        // Zapisanie wersji gry
        saveData.gameVersion = Managers.ConfigManager.gameVersion;
        
        // Zapisanie gracza
        foreach (var snake in GameObject.FindGameObjectsWithTag("Player"))
        {
            Snake snakeObject = snake.GetComponent<Snake>();
            
            if (snakeObject)
            {
                var position = snake.transform.position;
                saveData.snakeHeadPosition = new SerializableVec2(position.x, position.y);

                foreach (var body in snakeObject.snakeBody)
                    saveData.playerBodyPosition.Add(new SerializableVec2(body.transform.position.x, body.transform.position.y));

                saveData.tailPosition = new SerializableVec2(snakeObject.snakeTail.transform.position.x, snakeObject.snakeTail.transform.position.y);

                saveData.MoveDirection = snakeObject.MoveDirectionEnum;
                break;
            }
        }

        // Zapisanie jedzenia
        GameObject foodObject = GameObject.FindGameObjectWithTag("Food");

        if (foodObject != null)
            saveData.foodPosition = new SerializableVec2(foodObject.transform.position.x, foodObject.transform.position.y);
        
        // Zapisywanie przeciwnika
        GameObject enemyObject = GameObject.FindWithTag("Enemy");
        
        if (enemyObject != null)
            saveData.enemyPosition = new SerializableVec2(enemyObject.transform.position.x, enemyObject.transform.position.y);

        // Punkty

        if (Managers.LevelManager != null)
        {
            saveData.score = Managers.LevelManager.score;
            saveData.highscore = Managers.LevelManager.highscore; 
        }
        
        // Jezyk
        saveData.gameLanguage = (int)Managers.MultiLanguageManager.CurrentLanguage;

        // Zapisanie sterowanie
        saveData.snakeControl[0] = Managers.ConfigManager.upMoveKey;
        saveData.snakeControl[1] = Managers.ConfigManager.downMoveKey;
        saveData.snakeControl[2] = Managers.ConfigManager.leftMoveKey;
        saveData.snakeControl[3] = Managers.ConfigManager.rightMoveKey;
        
        // Glonosc gry
        saveData.audioVolume = Managers.AudioManager.GetVolumeLevel();
        
        playerProfile.saveData = saveData;
        
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, saveData);
        file.Close();
    }
}
