﻿using UnityEngine;

public class Node : MonoBehaviour
{
    [HideInInspector] public bool isAvailable; // Określa czy dane pole jest wolne (nie stoi na nim wąż ani żadna z jego części ciała)
    [HideInInspector] public bool isTurnNode; // Wąż skręcił ciałem tutaj
    [HideInInspector] public Snake.MoveDirection turnDirection; // Kierunek skrętu węża jaki dokonał na tym polu
    
    // Ustawia dane pole na null w tablicy z wolnymi polami
    public void RemoveFromAvailableList(GameObject node)
    {
        for (int i = 0; i < Managers.LevelManager.groundAvailableList.Length; i++)
        {
            if (Managers.LevelManager.groundAvailableList[i] != null)
            {
                if (node.name == Managers.LevelManager.groundAvailableList[i].name)
                {
                    Managers.LevelManager.groundAvailableList[i] = null;
                    return;
                } 
            }
        }
    }

    // Dodaje dane pole do tablicy z wolnymi polami
    public void AddToAvailableList(GameObject node)
    {
        for (int i = 0; i < Managers.LevelManager.groundAvailableList.Length; i++)
        {
            if (Managers.LevelManager.groundAvailableList[i] == null)
            {
                Managers.LevelManager.groundAvailableList[i] = node;
                return;
            } 
        }
    }
}
