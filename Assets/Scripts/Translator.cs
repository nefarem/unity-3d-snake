using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Translator : MonoBehaviour
{
    public string languageStringKey;
    
    // For previous version compatibility try set reference for 2 text components but only one will be.
    private Text text;
    private TextMeshProUGUI textMeshPro;
    
    private void Awake()
    {
        StartCoroutine(WaitForFinishLoading());
    }

    IEnumerator WaitForFinishLoading()
    {
        yield return new WaitUntil(() => Managers.GameManager.loadingFinish);
        
        text = GetComponent<Text>();
        textMeshPro = GetComponent<TextMeshProUGUI>();
        
        Managers.MultiLanguageManager.ChangeLanguage += OnChangeLanguage;
        Managers.MultiLanguageManager.OnChangeLanguage(Managers.MultiLanguageManager.CurrentLanguage, Managers.MultiLanguageManager.CurrentLanguage);
    }

    private void OnChangeLanguage(LanguageTypes oldLanguage, LanguageTypes newLanguage)
    {
        if (text != null)
            text.text = Managers.MultiLanguageManager.GetStringByKey(languageStringKey);
        else
            textMeshPro.text = Managers.MultiLanguageManager.GetStringByKey(languageStringKey);
    }
    
}
