using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Enemy : MonoBehaviour
{
    public GameObject foodPrefab;

    GameManager gameManager;
    AIDestinationSetter aIDestinationSetter; // A* pathfinding
    AILerp aILerp; // A* pathfinding

    void Start() 
    {
        aIDestinationSetter = GetComponent<AIDestinationSetter>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        aILerp = GetComponent<AILerp>();    
    }

    void Update() 
    {
        if (aIDestinationSetter.target == null) aIDestinationSetter.target = GameObject.FindGameObjectWithTag("Food").transform;

        if (gameManager.isGameOver || gameManager.isGamePause) aILerp.canMove = false;
        else aILerp.canMove = true;
    }

    public void ChangeTarget(Transform target)
    {
        aIDestinationSetter.target = target;
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.CompareTag("Ground"))
            other.gameObject.GetComponent<Node>().RemoveFromAvailableList(other.gameObject);

        if (other.CompareTag("Food"))
        {
            Destroy(other.gameObject);

            Food food = new Food(foodPrefab);
            aIDestinationSetter.target = food.GetFoodObject().transform;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ground")) other.GetComponent<Node>().AddToAvailableList(other.gameObject);
    }
}
