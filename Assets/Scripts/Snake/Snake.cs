﻿using System.Collections.Generic;
using UnityEngine;
public class Snake : MonoBehaviour, IMoving, ISetSprite
{
    [SerializeField] Sprite[] headSprites; // HeadDown, HeadLeft, HeadRight, HeadUp

    [SerializeField] GameObject bodyPrefab; // Prefab ciała wężą
    [SerializeField] GameObject foodPrefab; // Prefab jedzenia

    [SerializeField] AudioClip eatAudio; // Dźwięk zjedzenia jedzenia
    public GameObject snakeTail; // Ogon wężą

    [HideInInspector] public List<GameObject> snakeBody = new List<GameObject>(); // Części ciała węża
    
    [HideInInspector]  public List<Vector2> roadTurn = new List<Vector2>(); // Pozycje na których wąż zmieniał kierunek ruchu, gdy wszystkie części dot. ciała przebędą tę pozycję lista zostanie zresetowana
    
    public Vector2 previousPosition; // Poprzednia pozycja węża

    // Kierunki ruchu wężą
    public enum MoveDirection {left, right, up, down}
    public MoveDirection MoveDirectionEnum;
    
    bool isPossibleChangeDirection; // 'Opóźnienie' mające na celu nie pozwolenie graczowi wykonać szybko dwóch ruchów na raz
    GameObject enemyObject;
    GameObject actualGround; //Aktualny "tile" na którym jest głowa wężą
    AudioSource audioSource;
    SpriteRenderer spriteRenderer;
    Timer timer;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();

        timer = GameObject.FindObjectOfType<Timer>();
        // gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        // enemyObject = GameObject.Find("Enemy");
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        previousPosition = new Vector2(gameObject.transform.position.x, transform.position.y - 0.16f);
    }

    void Update()
    {
        // Zablokowanie ruchu jeżeli gra jest spauzowana lub zakończona
        Move();
        ChangeMoveDirection();
        // if (!Managers.GameManager.isGamePause && !Managers.GameManager.isGameOver)
        // {
        //     Move();
        //     
        //     if (isPossibleChangeDirection) 
        //         ChangeMoveDirection();
        // }

    }

    // Ruch wężą oraz jego części ciała
    public void Move()
    {
        if (timer.timerValue == timer.timerStartValue)
        {
            isPossibleChangeDirection = true;
            previousPosition = gameObject.transform.position;

            switch (MoveDirectionEnum)
            {
                case MoveDirection.up:
                    transform.position = new Vector2(transform.position.x, transform.position.y + 0.16f);
                    break;

                case MoveDirection.down:
                    transform.position = new Vector2(transform.position.x, transform.position.y - 0.16f);
                    break;

                case MoveDirection.left:
                    transform.position = new Vector2(transform.position.x - 0.16f, transform.position.y);
                    break;

                case MoveDirection.right:
                    transform.position = new Vector2(transform.position.x + 0.16f, transform.position.y);
                    break;                                        
            }

            // Ruszenie ciał wężą
            if (snakeBody.Count > 0)
            {
                for (int i = 0; i < snakeBody.Count; i++)
                    snakeBody[i].GetComponent<Body>().Move(i);
            }

            // Ruszenie ogona
            snakeTail.GetComponent<Tail>().Move();
        }         
    }

    // Ustawienie sprite'a w zależności od ruchu wężą
    public void SetSprite()
    {
        // Przy wczytywaniu gry które odbywa się wcześniej niż funkcja Awake nie ma odwołania do komponentu
        if (spriteRenderer == null) spriteRenderer = GetComponent<SpriteRenderer>();

        switch (MoveDirectionEnum)
        {
            case MoveDirection.up:
                spriteRenderer.sprite = headSprites[0];
                break;

            case MoveDirection.down:
                spriteRenderer.sprite = headSprites[1];
                break;

            case MoveDirection.left:
                spriteRenderer.sprite = headSprites[2];
                break;

            case MoveDirection.right:
                spriteRenderer.sprite = headSprites[3];
                break;                                        
        }      
    }

    // Zmiana kierunku ruchu  węża po naciśnięciu przycisków kierunku oraz zmiana sprite'a
    // Zablokowanie ruchu w przeciwnym kierunku z lewo na prawo i z góry na dół itp
    void ChangeMoveDirection()
    {
        if ((Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)) && MoveDirectionEnum != MoveDirection.down && MoveDirectionEnum != MoveDirection.up)
        {
            MoveDirectionEnum = MoveDirection.up;
            roadTurn.Add(gameObject.transform.position);
            actualGround.GetComponent<Node>().turnDirection = MoveDirectionEnum;
            actualGround.GetComponent<Node>().isTurnNode = true;
            SetSprite();
            isPossibleChangeDirection = false;
        }
            
        if ((Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S)) && MoveDirectionEnum != MoveDirection.up && MoveDirectionEnum != MoveDirection.down)
        {
            MoveDirectionEnum = MoveDirection.down;
            roadTurn.Add(gameObject.transform.position);
            actualGround.GetComponent<Node>().turnDirection = MoveDirectionEnum;
            actualGround.GetComponent<Node>().isTurnNode = true;
            SetSprite();
            isPossibleChangeDirection = false;
        }

        if ((Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)) && MoveDirectionEnum != MoveDirection.right && MoveDirectionEnum != MoveDirection.left)
        {
            MoveDirectionEnum = MoveDirection.left;  
            roadTurn.Add(gameObject.transform.position);
            actualGround.GetComponent<Node>().turnDirection = MoveDirectionEnum;
            actualGround.GetComponent<Node>().isTurnNode = true;

            SetSprite();
            isPossibleChangeDirection = false;
        }
               

        if ((Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)) && MoveDirectionEnum != MoveDirection.left && MoveDirectionEnum != MoveDirection.right) 
        {
            MoveDirectionEnum = MoveDirection.right; 
            roadTurn.Add(gameObject.transform.position);
            actualGround.GetComponent<Node>().turnDirection = MoveDirectionEnum;
            actualGround.GetComponent<Node>().isTurnNode = true;

            SetSprite();
            isPossibleChangeDirection = false;
        }
                             
    }

    // Zwiększenie rozmiaru węża po zjedzeniu jedzenia
    void GrowSnake(GameObject food)
    {
        Destroy(food);

        audioSource.clip = eatAudio;
        audioSource.Play();

        Managers.LevelManager.score++;
        timer.timeSpeed += 0.0001f; // przyspieszenie ruchu wężą

        // Zrespienie ciała wężą na pozycji na której znajdował się ogon, ogon natomiast przechodzi na swoją poprzednią pozycję
        GameObject body = Instantiate(bodyPrefab, snakeTail.transform.position, Quaternion.identity);
        body.GetComponent<Body>().previousPosition = snakeTail.transform.position;
        snakeBody.Add(body); // Dodanie ciała do listy z resztą

        snakeTail.transform.position = snakeTail.GetComponent<Tail>().previousPosition;
        snakeTail.GetComponent<Tail>().targetPosition = snakeTail.transform.position;
    }

    // Zrespienei jedzenia w losowym wolnym miejscu, jeżeli nie zostanie znalezione po przeszukaniu całej tablicy gra zostanie uznana za zakończoną
    void CreateNewFood()
    {
        Food food = new Food(foodPrefab);
        //enemyObject.GetComponent<Enemy>().ChangeTarget(food.GetFoodObject().transform);
    }

    // Gdy wąż wejdzie na pozycje z jedzeniem to powiększy się, na powierzchie to ustawiona będzie jako zajęta a uderzenie w inną swoją cześć ciała lub ściana oznacza koniec gry
    void OnTriggerEnter2D(Collider2D other) 
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.CompareTag("Food"))
        {
            GrowSnake(other.gameObject);
            CreateNewFood();
        }

        if (other.gameObject.CompareTag("Ground"))
        {
            actualGround = other.gameObject;
            actualGround.GetComponent<Node>().RemoveFromAvailableList(actualGround);
        }
            

        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Wall")) Managers.GameManager.GameOver(false, Managers.LevelManager.score, Managers.LevelManager.highscore);
        if (other.gameObject.CompareTag("Enemy"))  Managers.GameManager.GameOver(false, Managers.LevelManager.score, Managers.LevelManager.highscore);
    }
}
