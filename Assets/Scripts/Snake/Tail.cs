﻿using UnityEngine;

public class Tail : MonoBehaviour, IMoving
{
    public Vector2 previousPosition; // Poprzednia pozycja ogona

    [SerializeField]  Sprite[] tailSprites; // Sprite'y ogona

    public Vector2 targetPosition; // Cel 
    
    Snake snake;
    SpriteRenderer spriteRenderer;
    Body body;

    void Start()
    {
        snake = FindObjectOfType<Snake>();

        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Ruch ogona
    public void Move()
    {   
        previousPosition = gameObject.transform.position;

        // Jeżeli są częścia ciała węża powiększające go to ogon udawać się będzie do ostatniej pozycji, poprzedniej części ciała
        // W przeciwnym razie będzie udawać się do poprzedniej pozycji głowy wężą

        if (snake.snakeBody.Count > 0)
        {
            targetPosition = snake.snakeBody[snake.snakeBody.Count - 1].GetComponent<Body>().previousPosition;
            transform.position = targetPosition; 

            SetSprite(snake.snakeBody[snake.snakeBody.Count - 1].transform.position); 
        }
        else
        {
            targetPosition = snake.previousPosition;
            transform.position = targetPosition; 

            SetSprite(snake.transform.position); 
        }    
    }

    // Zmiana sprite'a w zależności od kierunku ruchu
    public void SetSprite(Vector2 targetPosition)
    {
        if (targetPosition.x == gameObject.transform.position.x)
        {
            if (targetPosition.y > gameObject.transform.position.y) spriteRenderer.sprite = tailSprites[0];
            else spriteRenderer.sprite = tailSprites[3];
        }
        else
        {
            if (targetPosition.x > gameObject.transform.position.x) spriteRenderer.sprite = tailSprites[2];
            else spriteRenderer.sprite = tailSprites[1];
        }
    }

    // Gdy ogon zejdzie z powierzchni zostanie ona ustawiona jako wolna i dodana do tablicy
    // Dodatkowo jeżeli była to pozycja na której waż skręcał zostanie usunięcia z listy
    void OnTriggerExit2D(Collider2D other) 
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            other.GetComponent<Node>().AddToAvailableList(other.gameObject);

            for (int i = 0; i < snake.roadTurn.Count; i++)
            {
                if (snake.roadTurn[i] == new Vector2(other.transform.position.x, other.transform.position.y))
                {
                    snake.roadTurn.Remove(snake.roadTurn[i]);
                    return;
                }
            }
        }
    }
}
