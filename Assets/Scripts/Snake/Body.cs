﻿using UnityEngine;

public class Body : MonoBehaviour, IMovingBody
{
    [HideInInspector] public Vector2 previousPosition; // Poprzednia pozycja
    [SerializeField] Sprite[] bodySprites; // Sprite'y ciała wężą

    [HideInInspector] public Vector2 targetPosition; // Pozycja na którą się ruszy

    SpriteRenderer spriteRenderer;
    Snake snake;

    GameObject actualGround; //Aktualny "tile" na którym jest ciało wężą
    GameObject snakeObject; // Głowa węża

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        snakeObject = GameObject.Find("Snake");
        snake = snakeObject.GetComponent<Snake>();

    }

    public void Move(int snakeBodyIndex)
    {   
        previousPosition = gameObject.transform.position;

        // Jeżeli część ciała ma indeks większy niż 0 czyli jest 2 lub dalszą od głowy to jej następnym celem będzie poprzednia pozycja ciała przed nim
        // W przeciwnym razie celem jest ostatnia pozycja głowy wężą
        if (snakeBodyIndex > 0 && snakeBodyIndex < 2)
        {
            targetPosition = snake.snakeBody[snakeBodyIndex-1].GetComponent<Body>().previousPosition;

            transform.position = targetPosition; 
            
            // Jeżeli pozycja była pozycją w której wąż skręcał to zmieniony zostanie sprite na odpowiedni z grafiką skrętu
            // W przeciwnym razie zostanie ustawiony sprite poziomy lub pionowy
            if (IsTurnPosition(gameObject.transform.position))
            {
                SetTurnSprite(snake.snakeBody[snakeBodyIndex-1].GetComponent<Body>().transform.position, snake.snakeTail.transform.position);
            }
            else SetSprite(snake.snakeBody[snakeBodyIndex-1].GetComponent<Body>().transform.position);
        }
        else if (snakeBodyIndex >= 2)
        {
            targetPosition = snake.snakeBody[snakeBodyIndex-1].GetComponent<Body>().previousPosition;

            transform.position = targetPosition;

            if (IsTurnPosition(gameObject.transform.position))
            {
                SetTurnSprite(snake.snakeBody[snakeBodyIndex-1].GetComponent<Body>().transform.position, snake.snakeBody[snakeBodyIndex-1].GetComponent<Body>().transform.position);
            }
            else SetSprite(snake.snakeBody[snakeBodyIndex-1].GetComponent<Body>().transform.position);           
        }
        else
        {
            targetPosition = snake.previousPosition;
            
            transform.position = targetPosition; 

            if (IsTurnPosition(gameObject.transform.position))
            {
                SetTurnSprite(snake.transform.position, snake.snakeTail.transform.position);
            }
            else SetSprite(snake.transform.position);

        } 

         
    }

    // Zmiana sprite'a dopasowana do pozycji następnej części ciała
    void SetSprite(Vector2 targetPosition)
    {
        if (!IsTurnPosition(gameObject.transform.position))
        {
            if (targetPosition.x == gameObject.transform.position.x) spriteRenderer.sprite = bodySprites[5];
            else spriteRenderer.sprite = bodySprites[2];
        }
    }

    // Zmiana sprite'a z grafiką skrętu do pozycji następnej części ciala
    void SetTurnSprite(Vector2 targetPosition, Vector2 previousBodyPosition)
    {
        // if (targetPosition.x == gameObject.transform.position.x)
        // {
        //     if (targetPosition.y > gameObject.transform.position.y)
        //     {
        //        if (previousBodyPosition.x > gameObject.transform.position.x) spriteRenderer.sprite = bodySprites[3];
        //        else spriteRenderer.sprite = bodySprites[4];
        //     }
        //     else
        //     {
        //        if (previousBodyPosition.x > gameObject.transform.position.x) spriteRenderer.sprite = bodySprites[1];
        //        else spriteRenderer.sprite = bodySprites[0];               
        //     } 
        // }

        // if (targetPosition.y == gameObject.transform.position.y)
        // {
        //     if (targetPosition.x > gameObject.transform.position.x)
        //     {
        //         if (previousBodyPosition.y > gameObject.transform.position.y) spriteRenderer.sprite = bodySprites[3];
        //         else spriteRenderer.sprite = bodySprites[1];
        //     }
        //     else
        //     {
        //         if (previousBodyPosition.y > gameObject.transform.position.y) spriteRenderer.sprite = bodySprites[4];
        //         else spriteRenderer.sprite = bodySprites[0];               
        //     }
        // }
        
        //Debug.Log(actualGround.GetComponent<Node>().turnDirection);

        foreach (var item in GameObject.FindGameObjectsWithTag("Ground"))
        {
            if (item.transform.position == gameObject.transform.position)
            {
                actualGround = item;
                break;
            }
        }

        if (actualGround.GetComponent<Node>().turnDirection == Snake.MoveDirection.down)
        {
            if (previousPosition.x < gameObject.transform.position.x) spriteRenderer.sprite = bodySprites[0];
            else spriteRenderer.sprite = bodySprites[1];
        }

        if (actualGround.GetComponent<Node>().turnDirection == Snake.MoveDirection.left)
        {
            if (previousPosition.y < gameObject.transform.position.y) spriteRenderer.sprite = bodySprites[0];
            else spriteRenderer.sprite = bodySprites[4];
        }

        if (actualGround.GetComponent<Node>().turnDirection == Snake.MoveDirection.right)
        {
            if (previousPosition.y < gameObject.transform.position.y) spriteRenderer.sprite = bodySprites[1];
            else spriteRenderer.sprite = bodySprites[3];
        }

        if (actualGround.GetComponent<Node>().turnDirection == Snake.MoveDirection.up)
        {
            if (previousPosition.x < gameObject.transform.position.x) spriteRenderer.sprite = bodySprites[4];
            else spriteRenderer.sprite = bodySprites[3];
        } 

    }

    // Sprawdzenie czy dana pozycja jest pozycją w której wąż dokonał skrętu
    bool IsTurnPosition(Vector2 bodyPosition)
    {
        for (int i = 0; i < snake.roadTurn.Count; i++)
        {
            if (snake.roadTurn[i] == bodyPosition) return true;
        }

        return false;
    }
}
