using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    GameObject foodObject;
    public Food(GameObject foodPrefab)
    {
        Vector2 availablePosition;
        int randomIndex = Random.Range(0, Managers.LevelManager.groundAvailableList.Length);
        int tmp = 0;

        foreach (var item in Managers.LevelManager.groundAvailableList)
        {
            if (item != null)
                if (item.GetComponent<Node>().isAvailable)
                {
                    tmp++;
                    break;
                }
        }

        if (tmp == 0)
        {
            Managers.GameManager.GameOver(true, Managers.LevelManager.score, Managers.LevelManager.highscore);
            return;
        }

        while (Managers.LevelManager.groundAvailableList[Random.Range(0, randomIndex)] == null)
        {
            randomIndex = Random.Range(0, Managers.LevelManager.groundAvailableList.Length);
        }
            

        availablePosition = Managers.LevelManager.groundAvailableList[randomIndex].transform.position;

        foodObject = Instantiate(foodPrefab, availablePosition, Quaternion.identity);
    }

    public GameObject GetFoodObject()
    {
        return foodObject;
    }
}
