﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverPanel : MonoBehaviour
{
    // Wywoływane na przycisku z końcem gry
    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void RestartGame()
    {
        PlayerPrefs.SetInt("GameProgress", 0);

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
