using System;
using System.IO;
using UnityEngine;

[Serializable]
public class LanguageStrings
{
    // MENU //
    public string continueGame;
    public string newGame;
    public string changeLanguage;
    public string exitGame;

    public string snakeControl;
    public string pauseMenu;
    public string gameDesc1;
    public string gameDesc2;
    public string resetHighscoreAsk;
    public string resetHighscoreYes;
    public string resetHighscoreNo;
    public string gameVersion;

    public string authorText;
    public string settingsButton;
    public string changesButton;
    public string roadmapButton;
    public string multiplayerButton;
    public string aboutButton;
    public string settingsMoveUp;
    public string settingsMoveDown;
    public string settingsMoveLeft;
    public string settingsMoveRight;
    public string settingsGameLanguage;
    
    // Game //

    // Kontynuacja jest już w menu i dotyczy tego samo
    public string backToMenu;
    public string youWin;
    public string youLose;
    public string resetGame;

    public static LanguageStrings CreateJSON(string path)
    {
        string json = File.ReadAllText(path);
        return JsonUtility.FromJson<LanguageStrings>(json);
    }

}
