using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResetHighscorePanel : MonoBehaviour
{
    [SerializeField] private Button yesButton;
    [SerializeField] private Button noButton;

    private void OnEnable()
    {
        yesButton.onClick.AddListener(OnYes);
        noButton.onClick.AddListener(OnNo);
    }
    
    private void OnYes()
    {
        Managers.ProfileManager.playerProfile.saveData.highscore = 0;

        StartCoroutine(ChangeScene());
    }
    
    private void OnNo()
    {
        StartCoroutine(ChangeScene());
    }

    public IEnumerator ChangeScene()
    {
        SceneManager.UnloadSceneAsync("Menu");
        
        SceneManager.LoadSceneAsync("SampleScene", LoadSceneMode.Additive);

        yield return null;
    }


}
