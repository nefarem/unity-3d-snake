using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPanel : MonoBehaviour
{
    public TMP_Dropdown[] moveDropdowns;
    public TMP_Dropdown changeLanguageDropdown;
    public Slider volumeSlider;
    
    private void OnEnable()
    {
        foreach (var moveDropdown in moveDropdowns)
        {
            moveDropdown.onValueChanged.RemoveAllListeners();

            switch (moveDropdown.GetComponent<MoveDropdownType>().moveDropdownEnumType)
            {
                case MoveDropdownType.MoveDropdownEnumType.Up:
                    moveDropdown.value = Managers.ConfigManager.upMoveKey;
                    break;
            
                case MoveDropdownType.MoveDropdownEnumType.Down:
                    moveDropdown.value = Managers.ConfigManager.downMoveKey;
                    break;
            
                case MoveDropdownType.MoveDropdownEnumType.Left:
                    moveDropdown.value = Managers.ConfigManager.leftMoveKey;
                    break;
            
                case MoveDropdownType.MoveDropdownEnumType.Right:
                    moveDropdown.value = Managers.ConfigManager.rightMoveKey;
                    break;
            }
            
            moveDropdown.onValueChanged.AddListener(OnMoveSettingChanged);
        }

        volumeSlider.value = Managers.ProfileManager.playerProfile.saveData.audioVolume;
        
        changeLanguageDropdown.onValueChanged.RemoveAllListeners();
        changeLanguageDropdown.onValueChanged.AddListener(OnChangeLanguage);
        volumeSlider.onValueChanged.AddListener(OnChangeVolume);
        
        changeLanguageDropdown.value = (int)Managers.MultiLanguageManager.CurrentLanguage;
    }

    private void OnChangeVolume(float value)
    {
        Managers.AudioManager.SetVolumeLevel(value);
        
        Managers.ProfileManager.SaveProfile();
    }

    private void OnMoveSettingChanged(int value)
    {
        foreach (var moveDropdown in moveDropdowns)
        {
            MoveDropdownType moveDropdownType = moveDropdown.GetComponent<MoveDropdownType>();

            switch (moveDropdownType.moveDropdownEnumType)
            {
                case MoveDropdownType.MoveDropdownEnumType.Up:
                    Managers.ConfigManager.upMoveKey = moveDropdown.value;
                    break;
            
                case MoveDropdownType.MoveDropdownEnumType.Down:
                    Managers.ConfigManager.downMoveKey = moveDropdown.value;
                    break;
            
                case MoveDropdownType.MoveDropdownEnumType.Left:
                    Managers.ConfigManager.leftMoveKey = moveDropdown.value;
                    break;
            
                case MoveDropdownType.MoveDropdownEnumType.Right:
                    Managers.ConfigManager.rightMoveKey = moveDropdown.value;
                    break;
            }
        }
        
        Managers.ProfileManager.SaveProfile();
    }
    
    private void OnChangeLanguage(int value)
    {
        Managers.MultiLanguageManager.SetLanguage((LanguageTypes)value);
    }
    
}
