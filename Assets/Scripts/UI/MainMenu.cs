﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{   
    [Header("UI Panels")]
    public GameObject menuPanelUI;
    public GameObject changelogPanelUI;
    public GameObject roadmapPanelUI;
    public GameObject settingsPanelUI;
    public GameObject aboutPanelUI;
    public GameObject resetHigscorePanelDecision;
    
    [Header("Buttons")]
    public Button newGameButton;
    public Button continueGameButton;
    public Button settingsButton;
    public Button changesButton;
    public Button roadmapButton;
    public Button aboutButton;
    public Button exitButton;

    public Button[] backToMenuButtons;
    
    void Start() 
    {   
        // Jeżeli poprzednia gra została przerwana w trakcie będzie możliwość kontynuowania i aktywowany przycisk w menu
        if (Managers.ProfileManager.playerProfile.saveData.canContinueGame) continueGameButton.interactable = true;
        else continueGameButton.interactable = false;
    
        // Add listeners
        newGameButton.onClick.AddListener(OnNewGame);
        continueGameButton.onClick.AddListener(OnContinueGame);
        settingsButton.onClick.AddListener(OnSettings);
        changesButton.onClick.AddListener(OnChanges);
        roadmapButton.onClick.AddListener(OnRoadmap);
        aboutButton.onClick.AddListener(OnAbout);
        exitButton.onClick.AddListener(OnExitGame);
        
        foreach (Button button in backToMenuButtons)
            button.onClick.AddListener(OnBackToMenu);
    }
    
    // Wywoływane na przyciskach w menu głównym
    private void OnNewGame()
    {
        resetHigscorePanelDecision.SetActive(true);
    }
 
    private void OnContinueGame()
    {
        ResetHighscorePanel resetHighscorePanel = resetHigscorePanelDecision.GetComponent<ResetHighscorePanel>();

        resetHighscorePanel.StartCoroutine(resetHighscorePanel.ChangeScene());
    }    
    
    private void OnSettings()
    {
        Managers.ProfileManager.LoadProfile();
        ChangeMainMenuActive(false);
        
        settingsPanelUI.SetActive(true);
    }
    
    private void OnChanges()
    {
        ChangeMainMenuActive(false);
        
        changelogPanelUI.SetActive(true);
    }
    
    private void OnRoadmap()
    {
        ChangeMainMenuActive(false);
        
        roadmapPanelUI.SetActive(true);
    }
    
    private void OnAbout()
    {
        ChangeMainMenuActive(false);
        
        aboutPanelUI.SetActive(true);
    }

    private void OnExitGame()
    {
        Application.Quit();
    }
    
    private void OnBackToMenu()
    {
        changelogPanelUI.SetActive(false);
        roadmapPanelUI.SetActive(false);
        settingsPanelUI.SetActive(false);
        aboutPanelUI.SetActive(false);
        
        ChangeMainMenuActive(true);
    }

    public void ChangeMainMenuActive(bool value)
    {
        menuPanelUI.SetActive(value);
    }
    
}
