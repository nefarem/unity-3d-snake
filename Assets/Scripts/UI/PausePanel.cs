using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePanel : MonoBehaviour
{
    GameManager gameManager;
    UIManager uIManager;

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        uIManager = GameObject.Find("GameManager").GetComponent<UIManager>();
    }

    // Wywoływane po kliknieciu przycisku w menu pauzy
    public void ContinueGame()
    {
        gameManager.isGamePause = false;
        uIManager.pauseMenu.SetActive(false);
    }

    public void BackToMenu()
    {
        // Przerwanie trwającej rozgrywki, zapisanie wszystkiego
        SaveGame();
        
        SceneManager.LoadScene("Menu");       
    }


    // Zapisywanie gry
    public void SaveGame()
    {
        SaveData save = SaveData();

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, save);
        file.Close();
    }

    SaveData SaveData()
    {
        SaveData saveData = new SaveData();

        // Zapisanie gracza
        foreach (var snake in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (snake.GetComponent<Snake>())
            {
                Snake snakeComponent = snake.GetComponent<Snake>();

                saveData.snakeHeadPosition = new SerializableVec2(snake.transform.position.x, snake.transform.position.y);

                foreach (var body in snake.GetComponent<Snake>().snakeBody)
                    saveData.playerBodyPosition.Add(new SerializableVec2(body.transform.position.x, body.transform.position.y));

                saveData.tailPosition = new SerializableVec2(snakeComponent.snakeTail.transform.position.x, snakeComponent.snakeTail.transform.position.y);

                saveData.MoveDirection = snake.GetComponent<Snake>().MoveDirectionEnum;
                break;
            }
        }

        // Zapisanei jedzenia
        GameObject foodObject = GameObject.FindGameObjectWithTag("Food");

        saveData.foodPosition = new SerializableVec2(foodObject.transform.position.x, foodObject.transform.position.y);

        // Zapisywanie przeciwnika

        saveData.enemyPosition = new SerializableVec2(GameObject.FindGameObjectWithTag("Enemy").transform.position.x, GameObject.FindGameObjectWithTag("Enemy").transform.position.y);

        saveData.score = Managers.LevelManager.score;

        return saveData;    
    }
}
