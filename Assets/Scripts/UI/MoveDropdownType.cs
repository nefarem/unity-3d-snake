using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MoveDropdownType : MonoBehaviour
{
    public MoveDropdownEnumType moveDropdownEnumType;
    public enum MoveDropdownEnumType
    {
        Up,
        Down,
        Left,
        Right
    }
    
}
