using System;

[Serializable]
public class PlayerProfile
{
    public SaveData saveData;
    
    public PlayerProfile(SaveData saveData)
    {
        this.saveData = saveData;
    }
}
